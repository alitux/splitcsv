# CSV Splitter

## Descripción

Esta es una utilidad de línea de comandos para dividir un archivo CSV en varios archivos más pequeños.

## Instalación

### Via Pip

```bash
pip install git+https://gitlab.com/alitux/splitcsv.git
```

### Vía Poetry

```bash
poetry add git+https://gitlab.com/alitux/splitcsv.git
```

## Uso

```bash
python csv_splitter.py -f <archivo_de_entrada> -p <numero_de_partes>
```

## Opciones

- -f, --file: El archivo CSV de entrada a dividir.
- -p, --parts: El número de partes en las que se dividirá el archivo. El valor predeterminado es 2.

## Ejemplo

```bash
python csv_splitter.py -f input.csv -p 4
```

Esto dividirá el archivo *input.csv* en 4 archivos más pequeños: *part_1.csv, part_2.csv, part_3.csv, y part_4.csv.*

## Nota

- Los archivos de salida se crearán en el mismo directorio que el script.
- El script no maneja errores, por lo que asegúrese de comprobar el archivo de entrada y el número de partes antes de ejecutar el script.

## Licencia

Este script está licenciado bajo la Licencia GNU/GPL. Consulte el archivo **LICENSE** para obtener más detalles.
