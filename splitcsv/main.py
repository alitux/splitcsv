import sys
import os
import pandas as pd
import argparse

def main():
    """
    Utilidad para dividir un archivo CSV en varios archivos csv mas pequeños
    
    Keyword arguments:
    --file -- Archivo a dividir(.csv)
    --parts -- partes en las que se dividirá el archivo
    Return: Varios archivos part_n según la cantidad definida en parts.
    """
    
    parser = argparse.ArgumentParser(description="Divide un archivo .csv en varios archivos mas pequeños")
    parser.add_argument('-f', '--file', type=str, help='Archivo a dividir')
    parser.add_argument('-p', '--parts', type=int, help='Partes en las que se dividirá el archivo. Por defecto 2 partes')
    args = parser.parse_args()
    if not any(vars(args).values()):
        parser.print_help()
        sys.exit(1)
    if args.file is not None:
        archivo_csv = os.path.join(os.path.dirname(__file__), args.file)

    if args.parts is not None:
        parts = args.parts
    if args.parts is None:
        parts = 2
    data = pd.read_csv(archivo_csv)

    total_filas = len(data)
    tamano_parte = total_filas // parts

    for i in range(parts):
        inicio = i * tamano_parte
        fin = (i + 1) * tamano_parte
        
        if i == parts-1:
            fin = total_filas
        
        parte = data.iloc[inicio:fin]
        
        nombre_archivo_parte = f'part_{i + 1}.csv'
        
        parte.to_csv(nombre_archivo_parte, index=False, header=True)

if __name__ == "__main__":
    main()